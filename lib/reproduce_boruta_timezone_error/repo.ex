defmodule ReproduceBorutaTimezoneError.Repo do
  use Ecto.Repo,
    otp_app: :reproduce_boruta_timezone_error,
    adapter: Ecto.Adapters.Postgres
end
