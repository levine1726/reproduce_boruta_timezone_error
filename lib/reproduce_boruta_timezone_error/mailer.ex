defmodule ReproduceBorutaTimezoneError.Mailer do
  use Swoosh.Mailer, otp_app: :reproduce_boruta_timezone_error
end
