defmodule ReproduceBorutaTimezoneErrorWeb.PageController do
  use ReproduceBorutaTimezoneErrorWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
